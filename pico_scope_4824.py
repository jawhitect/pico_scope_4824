
import ctypes
import numpy as np
from picosdk.ps4000a import ps4000a as ps
import matplotlib.pyplot as plt
from picosdk.functions import adc2mV, assert_pico_ok
from datetime import datetime

# The options for input voltage ranges about the center are allowed
rangeOptions = {
    '10mV': 0,
    '20mV': 1,
    '50mV': 2,
    '100mV': 3,
    '200mV': 4,
    '500mV': 5,
    '1V': 6,
    '2V': 7,
    '5V': 8,
    '10V': 9,
    '20V': 10,
    '50V': 11,
    '100V': 12,
    '200V': 13
}

# Available input channels to the picoscope 4824 and their number code.
Channels = {
    'A':0,
    'B':1,
    'C':2,
    'D':3,
    'E':4,
    'F':5,
    'G':6,
    'H':7
}

'''
This class hides the details of interacting with the picoscope to simplify data acquisition scripts.
'''
class pico_scope_4824:
    '''
    Initialize the pico_scope 4824 object. Create C integer to hold the handle to the device, and status dictionary.
    '''
    def __init__(self):
        # Create chandle and status ready for use
        self.chandle = ctypes.c_int16()
        self.status = {}
        self.channel_set = {}
        for channel in Channels.keys():
            self.channel_set[channel] = False

    '''
    Establish connection to the picoscope 4824
    '''
    def __enter__(self):
        print('Connecting to Picoscope 4824')
        self.status["openunit"] = ps.ps4000aOpenUnit(ctypes.byref(self.chandle), None)
        print("Result code:", self.status["openunit"])
        print("Scope handle:", self.chandle.value)

        try:
            assert_pico_ok(self.status["openunit"])
        except:

            powerStatus = self.status["openunit"]

            if powerStatus == 286:
                self.status["changePowerSource"] = ps.ps4000aChangePowerSource(self.chandle, powerStatus)
            else:
                raise

            assert_pico_ok(self.status["changePowerSource"])

        return self

    '''
    Close connection to the picoscope 4824
    '''
    def __exit__(self, exception_type, exception_value, traceback):
        print("Closing connection to pico scope 4824")
        self.status["close"] = ps.ps4000aCloseUnit(self.chandle)
        assert_pico_ok(self.status["close"])

        # display status returns
        print(self.status)

    #
    '''
    Sets of the Oscilloscope Channel
    More intuitive Wrapper for picosdk.ps4000a.ps4000aSetChannel()
    -----
    Inputs:
        channel - the channel of the oscilloscope that is being set up. Allowed Values:
                    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'
        enabled - Logical whether to enable the channel or not. Set True or 1 to enable, False or 0 to disale.
        coupling - set whether the Channel should be AC of DC coupled. Accepts 'AC' or 'ac' for AC coupling or 
                    'DC' or 'dc' for DC coupling.
        pmVRange - stands for '+/- voltage range,' is the voltage range of the channel about the center.
                If AC coupling, the center will be zero and pmVRange effectely becomes the maximum amplitude after
                any transients decay. If DC, the center will be the offset, so the range will be (Offset - pmVrange) 
                to (Offset + pmVrange).
        VOffset - The offset voltage from 0 from which to center the range of the cnannel. 
    '''
    def set_channel(self, channel, enabled, coupling, pmVRange, VOffset):

        # set up channel coupling for input into low level api function
        if (coupling == "AC") or (coupling == "ac"):
            coup = 0
        elif (coupling == "DC") or (coupling == "dc"):
            coup = 1
        else:
            raise ValueError(str(coupling)+" is not a valid coupling parameter. Must be 'ac' or 'dc'.\n")
        try:
            VRange_code = rangeOptions[pmVRange]
        except KeyError as ke:
            raise ValueError('%s is not an allowed range value. '%str(pmVRange)
                             +'Allowed values are: ' + ', '.join(rangeOptions.keys()))
        try:
            channel_num = Channels[channel]
        except KeyError as ke:
            raise ValueError('%s is not an allowed Channel. ' % str(pmVRange)
                             + 'Allowed channels are: ' + ', '.join(Channels.keys()))

        if enabled:
            enabled = 1
        else:
            enabled = 0

        self.status["setChA"] = ps.ps4000aSetChannel(self.chandle, channel_num, enabled, coup, VRange_code, VOffset)
        assert_pico_ok(self.status["setChA"])
        self.VRange_code = VRange_code
        self.channel_set[channel] = True



    '''
    Acquires a voltage time series with nPoints points separated by intervals of dt
    ----
    Input:
        dt - the time in between measurements in nanoseconds. Must be a multiple of 12.5 nanoseconds. See PicoScope® 4000 Series (A API) Programmers Guide section 3.7 page 17
    '''
    def acquire(self, nPoints, dt):
        if dt%12.5 != 0:
            raise ValueError('Error: only multiples of 12.5 nanoseconds are allowed.')
        timebase = int(dt/12.5 -1)

        if not self.channel_set['A']:
            raise(Exception("Must set Channel A before acquiring."))

        # get allowed nPoints and timebase from device
        timeIntervalns = ctypes.c_float()
        returnedMaxSamples = ctypes.c_int32()
        oversample = ctypes.c_int16(1)
        self.status["getTimebase2"] = ps.ps4000aGetTimebase2(self.chandle, timebase, nPoints, ctypes.byref(timeIntervalns), ctypes.byref(returnedMaxSamples), 0)
        if nPoints > returnedMaxSamples.value:
            raise ValueError("The maximum number of data points that may be acquired is %d. %d points were requested."%(returnedMaxSamples.value, nPoints))
        print('status: ', self.status["getTimebase2"])
        print('returnedMaxSamples: ', returnedMaxSamples.value)
        print('timeIntervalns: ', timeIntervalns.value)
        print('input dt: ', dt)
        assert(dt == timeIntervalns.value)

        self.status["runBlock"] = ps.ps4000aRunBlock(self.chandle, 0, nPoints, timebase, None, 0, None, None)
        assert_pico_ok(self.status["runBlock"])

        # Check for data collection to finish using ps4000aIsReady
        ready = ctypes.c_int16(0)
        check = ctypes.c_int16(0)

        while ready.value == check.value:
            self.status["isReady"] = ps.ps4000aIsReady(self.chandle, ctypes.byref(ready))

        bufferAMax = (ctypes.c_int16 * nPoints)()
        bufferAMin = (ctypes.c_int16 * nPoints)()  # used for downsampling which isn't in the scope of this example

        self.status["setDataBuffersA"] = ps.ps4000aSetDataBuffers(self.chandle, 0, ctypes.byref(bufferAMax),
                                                             ctypes.byref(bufferAMin), nPoints, 0, 0)
        assert_pico_ok(self.status["setDataBuffersA"])

        # create overflow locaction
        overflow = ctypes.c_int16()
        # create converted type maxSamples
        cmaxSamples = ctypes.c_int32(nPoints)
        self.status["getValues"] = ps.ps4000aGetValues(self.chandle, 0, ctypes.byref(cmaxSamples), 0, 0, 0,
                                                  ctypes.byref(overflow))
        assert_pico_ok(self.status["getValues"])

        # find maximum ADC count value

        maxADC = ctypes.c_int16(32767)

        # convert ADC counts data to mV
        adc2mVChAMax = adc2mV(bufferAMax, self.VRange_code, maxADC)

        self.status["stop"] = ps.ps4000aStop(self.chandle)
        assert_pico_ok(self.status["stop"])

        #return(adc2mVChAMax, timeIntervalns.value)
        return adc2mVChAMax

    def plot_acquire(self, nPoints, dt):
        v = self.acquire(nPoints, dt)
        tfinal = nPoints*dt
        if tfinal > 1000 and tfinal < 10**6:
            tfinal = tfinal/1000
            dt = dt/1000
            plt.xlabel('Time (microseconds)')
        elif tfinal > 10**6 and tfinal < 10**9:
            tfinal = tfinal/10**6
            dt = dt/10**6
            plt.xlabel('Time (ms)')
        else:
            plt.xlabel('Time (ns)')

        plt.plot(np.arange(0, tfinal, dt), v)

        plt.ylabel('Voltage (mV)')
        plt.title('Picoscope Trace')
        plt.tight_layout()
        plt.show()

    def plot_fft(self, nPoints, dt,sFreq, eFreq):
        v = self.acquire(nPoints, dt)
        dt = dt/10**9 #convert to seconds
        fft = np.fft.fft(v)
        freqs = np.fft.fftfreq(nPoints,dt)
        mask = freqs > 0
        freqs = freqs[mask]
        fft = fft[mask]
        sFreqind = np.argmin(np.abs(freqs-sFreq))
        eFreqind = np.argmin(np.abs(freqs-eFreq))
        print(sFreqind, sFreqind)
        #v = v[mask]
        plt.plot(freqs[sFreqind:eFreqind], fft[sFreqind:eFreqind])




def timestamp():
    return '{:%Y%m%d_%Hh%Mm%Ss}'.format(datetime.today())